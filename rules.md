## Article ? - The Council
##### § IV - Procedure
**I** Rules
**II** Mandatory Meetings, after the first meeting, shall proceed as follows:
- Meeting shall be called to order by the Chancellor and a roll call taken of all members. If less than one-third of the Council appears for the meeting, the meeting shall be called void
- Minutes of the last meeting shall be read by the Time-Keeper
- Chancellor shall offer the floor to Council Members to bring to light any new business needed to be addressed by the Council.
- When all new business is settled, the Council shall take up a meal
- Upon the conclusion of the meal, the chancellor shall offer up the notion to adjourn
- Two more Council Members must Second and Third the notion for it to pass
- Chancellor will adjourn the meeting
**III**	Rules for location of meetings


##### § II - Admission
**III** Once the requesting individual has completed their Request of Admission, the requesting individual will be taken outside of the Council Hall so that deliberations can begin.
**IV** 
**V** When deliberations have finished, the Council will then put the Request of Admission to a vote by all present members that are in good standing with the Council. In order for the Request of Admission to be approved, it requires a Simple Majority by the Council Members.
**VI** After the Council has completed their vote, the Council Meeting will be placed on hold while the requesting individual is invited back into the Council Hall. At this time the Supreme Chancellor, or other appointed Officer of the Council, will convey the result of the Council’s Request of Admission vote. 
Deliberations of the Council shall extend until such time that all Members feel confident that their likes, dislikes, and concerns for the requesting individual have been addressed. 

##### § V - Officers
**I** Chancellor – Figure Head of the Council, and in charge of most day-to-day responsibilities and decisions on the Council’s behalf, that are in the Council’s best interest. Elected position every year.
**II** Record Keeper – Charged with keeping records of all Council Meetings, along with any Council paperwork, forms, property, etc. Elected position every second year.
**III**	Council Elders – Elected Officials whom are wise and understanding in ways beyond the mortal realm. They guide, and advise the Chancellor and Council Members during troubled times and decisions. Elders are elected at the first meeting of the Council and are appointed for life.

## Article ? - Meetings
**I**	This Council shall hold mandatory meetings of at least, but not limited to, one meeting per quarter or four meetings per calendar year with at least two-thirds of the Council present at each meeting. The first mandatory meeting of the Council will house the following:
A: 	Each Founding Member must place their name, contact information, address, and whatever else may be deemed necessary in the Council Database.
		B: 	Once each Founding Member has done the above, the election process will begin.
1.	The Founding Members must first vote on ratifying this Charter
2.	The Founding Members will elect Council Officers:
a.	A Ballot Officer must be selected, this will be whichever Founding Member, or Founding Representative, that wishes the responsibility, or by vote if there are multiple volunteers
b.	To be put on the Council Officers Ballot, a Founding Member’s candidacy must be offered up by themselves, and seconded by another member
c.	Once all members names are placed on the Ballot, an anonymous vote shall be casted by all Founding Members, including those on the Ballot and the Ballot Officer
d.	Once all members have casted votes for Council Officers, the Ballot Officer will take the votes and count them in the Ballot Room of the Council Chambers, the Ballot Officer will announce the victors of the Elections, and his/her term will be renounced as the Elections have concluded.
e.	Any person(s) that violate the trust of the Election, try to rig elections, or bribe any candidates shall be punished under the rules and regulations of Article VI.