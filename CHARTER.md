## Charter between the free peoples of Earth

**Table of Contents**

[Preamble](charter/preamble.md)

[Article I - The Council](charter/council.md)

[Article II - Legislature](charter/legislature.md)

[Article III - Executive](charter/executive.md)

[Article IV - Judiciary](charter/judiciary.md)

[Article V - Impeachment](charter/impeachment.md)

[Article VI - Expulsion](charter/expulsion.md)

[Article VII - Protections of the People](charter/protections.md)

[Article VIII - Agreements between the People](charter/agreement.md)

[Article IX - Amendments](charter/amendments.md)

[Article X - Ratification](charter/ratification.md)

**Example**
## Article I - Article
#### § I - Section
**I** Clause

1. Paragraph.
  
  * Subparagraph  
  
  * Subparagraph

2. Paragraph.