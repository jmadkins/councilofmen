## Article III - Executive
#### Section I - Supreme Chancellor
**I** The executive Power shall be vested in a Supreme Chancellor of the Council. The occupant shall hold their Office during the Term of one Year and shall be elected by the Council.

**II** No Member shall hold the office of the Supreme Chancellor for a period longer than two successive terms of one year. Terms shall be considered successive unless separated by a period of one or more years.

**III** Before he enter on the Execution of his Office, he shall take the following Oath or Affirmation: -- "I do solemnly swear (or affirm) that I will faithfully execute the Office of Supreme Chancellor of the Council, and will to the best of my Ability, preserve, protect and defend the Charter between the free peoples of Earth with the awesome Powers of the Council."

#### Section II - Succession
**I** In case of the removal of the Supreme Chancellor from office or of his death, or resignation the Chief Elder of the Court of Elders shall serve as Acting Supreme Chancellor until a new Supreme Chancellor has been elected.

**II** Whenever the Supreme Chancellor transmits to the Record Keeper his written declaration that he is unable to discharge the powers and duties of his office, and until he transmits to them a written declaration to the contrary, such powers and duties shall be discharged by the Chief Elder as Acting Supreme Chancellor.

**III** Whenever a majority of either the principal officers of the executive department or of such other body as the Council may by Statute provide:

1. Transmit to the Record Keeper their written declaration that the Supreme Chancellor is unable to discharge the powers and duties of his office, the Chief Elder shall immediately assume the powers and duties of the office as Acting Supreme Chancellor.

2. Thereafter, when the Supreme Chancellor transmits to the Record Keeper his written declaration that no inability exists, he shall resume the powers and duties of his office unless a majority of either the principal officers of the executive department or of such other body as the Council may by Statute provide, transmit within four days to the Record Keeper their written declaration that the Supreme Chancellor remains unable to discharge the powers and duties of his office.

3. Thereupon, the Court of Elders shall decide the issue, assembling within forty-eight hours for that purpose if not in session.

   - If the Court, within twenty-one days after receipt of the latter written declaration determines by two-thirds that the Supreme Chancellor is unable to discharge the powers and duties of his office, the Chief Elder shall continue to discharge the same as Acting Supreme Chancellor; otherwise, the Supreme Chancellor shall resume the powers and duties of his office.

   - If the Court is not in session, within twenty-one days after the Court is required to assemble the Chief Elder shall continue to discharge the same as Acting Supreme Chancellor.   

#### Section III - Powers
**I** The Supreme Chancellor shall be Commander in Chief of any Forces in service for the Council.

**II** With the Advice and Consent of the Court of Elders, the Supreme Chancellor shall have the power to:

1. Appoint Advisers to oversee various aspects of the administration of the Council. They shall serve at the pleasure of the Supreme Chancellor and their term will expire with the term of the Chancellor.

2. Make Treaties, provided two-thirds of the voting Elders present concur;

3. Appointment Ambassadors, other public Ministers and Consuls, and all other Officers of the Council, whose Appointments are not herein otherwise provided for, and which shall be established by Statute: but the Council may by Statute vest the Appointment of such inferior Officers, as they think proper, in the Supreme Chancellor alone, or in the Heads of Committee.

**III** With the Advice and Consent of two-thirds majority of the Council, the Supreme Chancellor shall have the power to:

1. Appointment Members to the Court of Elders.

**IV** On his own accord, the Supreme Chancellor shall have the power to:

1. Fill up all Vacancies that may happen during the Recess of the Council, by granting Commissions which shall expire at the End of their next Session.

2. On extraordinary Occasions, demand the Council to convene.

3. Require the Opinion, in writing, of the principal Officer in each of the executive Committees, upon any Subject relating to the Duties of their respective Offices.

4. Grant Reprieves and Pardons for Offenses against the Council, except in Cases of Impeachment.

#### Section IV - Duties

**I** He shall from time to time give to the Council information of the State of the Council, and recommend to their Consideration such Measures as he shall judge necessary and expedient.

**II** He shall receive Ambassadors and other public Ministers.

**III** He shall take Care that the Statutes be faithfully executed.

**IV** He shall Commission all the Officers of the Council.