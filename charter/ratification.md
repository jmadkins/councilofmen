## Article X - Ratification
#### Section I
**I** This Charter between the free peoples of Earth shall be established between the Persons so ratifying the Same.

**II** Each free Person ratifying is granted automatic status as a Member of the Council or Honored Representative of the Council.