## Article IV - Judiciary
#### Section I - Court of Elders
**I** The judicial Power of the Council, shall be vested in the Court of Elders. The Elders shall hold their Offices for life and during good behavior. Each shall have one Vote on Questions before the Court.

**II** Elders shall choose their Chief Elder and other Officers.

**III** A majority of Elders shall constitute a Quorum to do Business; but a smaller Number of Elders may adjourn from day to day, and may be authorized to compel the Attendance of absent Elders, in such Manner, and under such Penalities as the Court may provide.

**IV** The Court shall determine the Rules of its Proceedings, punish Elders for disorderly Behavior, and, with the Concurrence of two-thirds, expel a Member.

#### Section II - Judicial Powers

**I** All Cases, in Statute and Equity, arising under this Charter, the Statutes of the Council, and Treaties made, or which shall be made, under their Authority.

**II** All Cases affecting Ambassadors, other public Ministers and Consuls.

**III** All Cases of admiralty and maritime Jurisdiction.

**IV** Controversies to which the Council shall be a Party.

**V** Controversies between two or more persons; and between Members, Representatives, or Offices, and foreign States, Citizens or Subjects.

#### Section III - Additional Powers

**I** If the Elders unanimously agree that a proposed Bill before the Council would be in violation of the Charter, said Bill will not be allowed to proceed until such concerns are satisfied.

**II** Elders shall have the Power to introduce legislation or Amendments before the Council but shall have no Vote on the matter unless they come before the Court.

#### Section IV - Proceedings

**I** The Trial of all Crimes, except in Cases of Impeachment, shall be by Jury; and such Trial shall be held in the Member or Representative's Home where the said Crimes shall have been committed; but when not committed within any Member or Representative's Home, the Trial shall be at such Place or Places as the Council may by Statue have directed.

#### Section V - Treason

**I** Treason against the Council, shall consist only in levying War against them, or in adhering to their Enemies, giving them Aid and Comfort. No Person shall be convicted of Treason unless on the Testimony of two Witnesses to the same overt Act, or on Confession in open Court.

**II** The Council shall have Power to declare the Punishment of Treason, but no Attainder of Treason shall work Corruption of Blood, or Forfeiture except during the Life of the Person attainted.