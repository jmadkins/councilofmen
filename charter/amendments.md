## Article IX - Amendments
#### Section I
**I** Whenever a Member of the Council deems it necessary, they shall propose Amendments to this Charter, which, shall be valid to all Intents and Purposes, as Part of this Charter when ratified.

**II** Ratification of an Amendment requires the approval of three-fourths of the Council as well as the approval of three-fourths of the Court.

**III** Amendments do not need the consent of the Supreme Chancellor.

**IV** No Amendment in any Manner may:

1. Deprive a Member or Representative of their Suffrage in the Council, excluding when an elected Officer.

2. Alter Article Seven, Section One, Clause Three.