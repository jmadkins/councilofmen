## Article II - Legislature
#### Section I
**I** All legislative Powers herein are granted to the Council itself.

**II** Members and Representatives of the Council shall have one Vote on Questions before the Council.

**II** A majority of Members shall constitute a Quorum to do Business; but a smaller Number of Members may adjourn from day to day, and may be authorized to compel the Attendance of absent Members, in such Manner, and under such Penalities as the Council may provide.

**III** The Council shall determine the Rules of its Proceedings, punish its Members for disorderly Behavior, and, with the Concurrence of two-thirds, expel a Member.

**IV** The Council shall keep a Journal of its Proceedings, and from time to time publish the same, excepting such Parts as may in their Judgment require Secrecy; and the Yeas and Nays of the Members on any question shall, at the Desire of one fifth of those Present, be entered on the Journal.

**V** Each Member, except the Supreme Chancellor, shall receive one vote on Questions before the Council. The Supreme Chancellor shall also have no Vote, unless the Council or Court be equally divided.

**VI** Voting Members shall choose their Record Keeper and other Officers for the Term of two Years.

#### Section II - Powers
**I** The Council shall have the Power to:

1. Lay and collect Taxes, Duties, Imposts and Excises, to pay the Debts and provide for the common Defense and general Welfare of the Council; but all Duties, Imposts and Excises shall be uniform throughout the Council.

2. Borrow Money on the credit of the Council.

3. Regulate Commerce with foreign persons and among the several Members.

4. Ensure that slavery nor involuntary servitude exist within jurisdiction of the Council.

5. Set the Times, Places, and Manner of holding Elections.

6. Grant Letters of Marque and Reprisal, and make Rules concerning Captures on Land and Water.

7. Promote the Progress of Comedy and useful Jokes, by securing for limited Times to Authors and Inventors the exclusive Right to their respective Writings and Discoveries;

8. Dispose of and make all needful Rules and Regulations respecting the Territory or other Property belonging to the Council; and nothing in this Charter shall be so construed as to Prejudice any Claims of the Council, or of any particular Member.

9. Make all Statutes which shall be necessary and proper for carrying into Execution the foregoing Powers, and all other Powers vested by this Charter in the Council, or in any Committee or Officer thereof.

**II** With the Advice and Consent of the Court of Elders, the Council shall have the power to:

1. Declare War

**III** The Council shall not have the Power to:

1. Establish a religion, or prohibit the free exercise thereof.

2. Abridge the freedom of speech, or of the press; or the right of the Members peaceably to assemble, and to petition the Council for a redress of grievances.

3. In time of peace, quarter persons in any house without the consent of the Owner and not in time of war, but in manner to be prescribed by Statute.

4. Deprive any person of life, liberty, or property, without due process of Statute or deny to any person within its jurisdiction the equal protection of the Statute.

#### Section III - Bills
**I** Every Bill which shall have passed the Council, shall, before it become a Statute, be presented to the Supreme Chancellor of the Council; if he approves he shall sign it, but if not he shall return it, with his Objections to the Council, who shall enter the Objections at large on their Journal, and proceed to reconsider it.

**II** If after such Reconsideration two-thirds of the Council shall agree to pass the Bill, it shall become a Statute. If any Bill shall not be returned by the Supreme Chancellor within ten Days after it shall have been presented to him, the Same shall be a Statute, in like Manner as if he had signed it, unless the Council by their Adjournment prevent its Return, in which Case it shall not be a Statute.