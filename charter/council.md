## Article I - The Council
#### Section I - Nomenclature
**I** The Stile of this Convention shall be “The Council.”

**II** All persons who have sworn fealty to The Council are to bear the title of “Member of the Council,” hereby referred to as "Councilmember", or simply “Members.”

**III** All Members who hold elected Office shall also be refereed to as as Officers.

#### Section II - Admission
**I** New persons may be admitted by the Council into this Convention after notifying the Record Keeper of the Council by letter.

**II** The Council shall have the opportunity to question the applicant and deliberate upon the Petition with a majority of all voting Members approval required for admission.

**III** The following conditions shall disqualify one from being Admitted to this Convention, however, the Court shall have the power to remove such disability with two-thirds majority of the members present:

1. Engaged in insurrection or rebellion against the Council, or given aid or comfort to the enemies thereof.

#### Section III - Withdraw
**I** If at anytime a person wishes to end relations with the Council, he or she may do so by notifying the Record Keeper of the Council by letter.

**II** No restrictions are applied to former Members or Representatives of the Council wishing to Petition for Admission again.

#### Section IV - Meeting
**I** The Council shall assemble at least once every Year on the third Sunday in the month of May to hold all Elections and conduct any required business.