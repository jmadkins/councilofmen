## Article V - Impeachment
#### Section I
**I** The Supreme Chancellor, Chief Elder, Record Keeper, and all Officers of the Council, shall be removed from Office on Impeachment for, and Conviction of, Treason, Bribery, or other high Crimes and Misdemeanors.

**II** The Court of Elders shall have the sole Power to try all Impeachments. When sitting for that Purpose, they shall be on Oath or Affirmation. No Person shall be convicted without the Concurrence of two-thirds of the Elders present.

**III** Judgment in Cases of Impeachment shall not extend further than to removal from Office, and disqualification to hold and enjoy any Office of honor, Trust or Profit under the Council: but the Party convicted shall nevertheless be liable and subject to Indictment, Trial, Judgment and Punishment, according to Statutes.

**IV** Any Member of the Council may introduce Articles of Impeachment against an Officer. No Articles shall be referred to the Court for debate and conviction without the Concurrence of a majority of the Members present.