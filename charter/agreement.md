## Article VIII - Agreements between the People
#### Section I
**I** This Charter, and the Statutes of the Council which shall be made in Pursuance thereof; and all Treaties made, or which shall be made, under the Authority of the Council, shall be the supreme Statutes of the Council.

**II** The Members and Representatives before mentioned, and all executive and judicial Officers of the Council, shall be bound by Oath or Affirmation, to support this Charter; but no religious Test shall ever be required as a Qualification to any Office or public Trust under the Council.

**III** No provision within this Charter can be enforced in a Court of Law within the jurisdiction of the following landmasses: Africa, Asia, Australia, Europe, North America, and South America. Membership in this Convention does not reflect real world views.

#### Section II
**I** No persons shall enter into any Treaty, Alliance, or Confederation; grant Letters of Marque and Reprisal; pass any Bill of Attainder, or Statutes impairing the Obligation of Contracts, or grant any Title of Nobility.

**II** No persons shall, without the Consent of the Council, keep Troops, or Ships of War in time of Peace, enter into any Agreement or Compact with a foreign Power, or engage in War, unless actually invaded, or in such imminent Danger.