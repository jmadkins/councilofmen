## Article VII -  Protections of the People
#### Section I
**I** The powers not delegated to the Council by the Charter, are reserved to the people.

**II** The enumeration in this Charter, of certain rights, shall not be construed to deny or disparage others retained by the people.

**III** The Council shall have power to enforce, by appropriate legislation, the provision of this article.

#### Section II
**I** The right of the Members to be secure in their persons, houses, papers, and effects, against unreasonable searches and seizures, shall not be violated, and no Warrants shall issue, but upon probable cause, supported by Oath or affirmation, and particularly describing the place to be searched, and the persons or things to be seized.

**II** Excessive bail shall not be required, nor excessive fines imposed, nor cruel and unusual punishments inflicted.

**III** The right of Members of the Council to vote shall not be denied or abridged on any account.