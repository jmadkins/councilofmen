## Article VI - Expulsion
#### Section I
**I** All Members or Representatives of the Council, shall be Expelled from the Council on Conviction of, Treason, Bribery, or other high Crimes and Misdemeanors.

**II** The Court of Elders shall have the sole Power to try all Expulsions. When sitting for that Purpose, they shall be on Oath or Affirmation. No Person shall be expelled without the Concurrence of three-fourths of the Elders present.

**III** Any Member of the Council may introduce Articles of Expulsion against another person. Articles introduced in the Council shall not be referred to the Court of Elders for debate without the Concurrence of a one-third majority of the Elders present.

**IV** The Court of Elders shall have the power to introduce and debate Articles of Expulsion if one-fourth majority of all Council Elders agree.